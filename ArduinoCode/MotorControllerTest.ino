enum class HalfBridgeId
{
  U = 0,
  V = 1, 
  W = 2
};

enum class HalfBridgeState
{
  HIGH_SIDE = 0,
  LOW_SIDE = 1, 
  DISABLED = 2
};

enum class HallSensor
{
  A = 0,
  B = 1,
  C = 2
};

/*
 * 1. EN high and IN low turns low side on
 * 2. EN high and IN high turns high side on
 * 3. If EN is low, the half bridge is disabled.
 * 
 * PD0 -> IN1
 * PD1 -> IN2
 * PD2 -> IN3
 * PD3 -> EN1
 * PD4 -> EN2
 * PD5 -> EN3
 */

void SetHalfBridge(HalfBridgeId bridge, HalfBridgeState state)
{
  uint8_t IN_pin_number = (int) bridge;
  uint8_t EN_pin_number = ((int) bridge)+3;

  switch(state)
  {
    case HalfBridgeState::HIGH_SIDE:
      digitalWrite(IN_pin_number, HIGH);
      digitalWrite(EN_pin_number, HIGH);
      break;
      
    case HalfBridgeState::LOW_SIDE:
      digitalWrite(IN_pin_number, LOW);
      digitalWrite(EN_pin_number, HIGH);
      break;
      
    case HalfBridgeState::DISABLED:
      digitalWrite(EN_pin_number, LOW);
      digitalWrite(IN_pin_number, LOW);
      break;
  }
};

/**
* 1. Hall C high -> U high, V low, W disabled
* 2. Hall A, C high -> U high, V disabled, W low
* 3. Hall A high -> U disabled, V high, W low
* 4. Hall A, B high -> U low, V high, W disabled
* 5. Hall B high -> U low, V disabled, W high
* 6. Hall B, C high -> U disabled, V low, W high
*/

HalfBridgeState SensorStatesToCCWHalfBridgeStates[2][2][2][3]; //A state, B state, C state -> UVW states


void SetTrapizoidal(bool A, bool B, bool C, bool CCW, bool highSideOn)
{
  HalfBridgeState CCW_UVW_states[3] = {SensorStatesToCCWHalfBridgeStates[(uint8_t) A][(uint8_t) B][(uint8_t) C][0], SensorStatesToCCWHalfBridgeStates[(uint8_t) A][(uint8_t) B][(uint8_t) C][1], SensorStatesToCCWHalfBridgeStates[(uint8_t) A][(uint8_t) B][(uint8_t) C][2]};

  HalfBridgeState UVW_states[3];
  UVW_states[0] = CCW_UVW_states[0];
  UVW_states[1] = CCW_UVW_states[1];
  UVW_states[2] = CCW_UVW_states[2];
  
  if(!CCW)
  {
    //Flip high/low side (should check this logic)
    for(uint8_t index = 0; index < 3; index++)
    {
      if(CCW_UVW_states[index] == HalfBridgeState::HIGH_SIDE)
      {
        UVW_states[index] = HalfBridgeState::LOW_SIDE;
      }
      else if(CCW_UVW_states[index] == HalfBridgeState::LOW_SIDE)
      {
        UVW_states[index] = HalfBridgeState::HIGH_SIDE;
      }
    }
  }

  //If the high side isn't suppose to be on, disable any high side commands found
  for(uint8_t index = 0; index < 3; index++)
  {
    if(UVW_states[index] == HalfBridgeState::HIGH_SIDE)
    {
      UVW_states[index] = HalfBridgeState::DISABLED;
    }
  }

  //Apply states
  for(uint8_t index = 0; index < 3; index++)
  {
    SetHalfBridge((HalfBridgeId) index, UVW_states[index]);
  }
}

void setup() 
{
  //Setup IN/EN output pins
  for(uint8_t pin_number = 0; pin_number < 6; pin_number++)
  {
    pinMode(pin_number, OUTPUT);
    digitalWrite(pin_number, LOW);
  }

  //Setup map from sensor states to desired half bridge states

  //Error state, so disable everything
  SensorStatesToCCWHalfBridgeStates[0][0][0][0] = HalfBridgeState::DISABLED;
  SensorStatesToCCWHalfBridgeStates[0][0][0][1] = HalfBridgeState::DISABLED;
  SensorStatesToCCWHalfBridgeStates[0][0][0][2] = HalfBridgeState::DISABLED;

  //Error state, so disable everything
  SensorStatesToCCWHalfBridgeStates[1][1][1][0] = HalfBridgeState::DISABLED;
  SensorStatesToCCWHalfBridgeStates[1][1][1][1] = HalfBridgeState::DISABLED;
  SensorStatesToCCWHalfBridgeStates[1][1][1][2] = HalfBridgeState::DISABLED;

  //Hall C high -> U high, V low, W disabled
  SensorStatesToCCWHalfBridgeStates[0][0][1][0] = HalfBridgeState::HIGH_SIDE;
  SensorStatesToCCWHalfBridgeStates[0][0][1][1] = HalfBridgeState::LOW_SIDE;
  SensorStatesToCCWHalfBridgeStates[0][0][1][2] = HalfBridgeState::DISABLED;

  //Hall A, C high -> U high, V disabled, W low
  SensorStatesToCCWHalfBridgeStates[1][0][1][0] = HalfBridgeState::HIGH_SIDE;
  SensorStatesToCCWHalfBridgeStates[1][0][1][1] = HalfBridgeState::DISABLED;
  SensorStatesToCCWHalfBridgeStates[1][0][1][2] = HalfBridgeState::LOW_SIDE;

  //Hall A high -> U disabled, V high, W low
  SensorStatesToCCWHalfBridgeStates[1][0][0][0] = HalfBridgeState::DISABLED;
  SensorStatesToCCWHalfBridgeStates[1][0][0][1] = HalfBridgeState::HIGH_SIDE;
  SensorStatesToCCWHalfBridgeStates[1][0][0][2] = HalfBridgeState::LOW_SIDE;

  //Hall A, B high -> U low, V high, W disabled
  SensorStatesToCCWHalfBridgeStates[1][1][0][0] = HalfBridgeState::LOW_SIDE;
  SensorStatesToCCWHalfBridgeStates[1][1][0][1] = HalfBridgeState::HIGH_SIDE;
  SensorStatesToCCWHalfBridgeStates[1][1][0][2] = HalfBridgeState::DISABLED;

  //Hall B high -> U low, V disabled, W high
  SensorStatesToCCWHalfBridgeStates[0][1][0][0] = HalfBridgeState::LOW_SIDE;
  SensorStatesToCCWHalfBridgeStates[0][1][0][1] = HalfBridgeState::DISABLED;
  SensorStatesToCCWHalfBridgeStates[0][1][0][2] = HalfBridgeState::HIGH_SIDE;

  //Hall B, C high -> U disabled, V low, W high
  SensorStatesToCCWHalfBridgeStates[0][1][1][0] = HalfBridgeState::DISABLED;
  SensorStatesToCCWHalfBridgeStates[0][1][1][1] = HalfBridgeState::LOW_SIDE;
  SensorStatesToCCWHalfBridgeStates[0][1][1][2] = HalfBridgeState::HIGH_SIDE;

  pinMode(LED_BUILTIN, OUTPUT);

  SetHalfBridge(HalfBridgeId::U, HalfBridgeState::LOW_SIDE);
  SetHalfBridge(HalfBridgeId::V, HalfBridgeState::LOW_SIDE);
  SetHalfBridge(HalfBridgeId::W, HalfBridgeState::LOW_SIDE);

  delay(1000);
}

//Progress through the hall states
/**
* 1. Hall C high -> U high, V low, W disabled
* 2. Hall A, C high -> U high, V disabled, W low
* 3. Hall A high -> U disabled, V high, W low
* 4. Hall A, B high -> U low, V high, W disabled
* 5. Hall B high -> U low, V disabled, W high
* 6. Hall B, C high -> U disabled, V low, W high
*/

void loop() 
{
  bool hall_states[6][3] = {{false, false, true}, {true, false, true}, {true, false, false}, {true, true, false}, {false, true, false}, {false, true, true}};


digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));


SetHalfBridge(HalfBridgeId::V, HalfBridgeState::DISABLED);
SetHalfBridge(HalfBridgeId::W, HalfBridgeState::LOW_SIDE);
SetHalfBridge(HalfBridgeId::U, HalfBridgeState::HIGH_SIDE);
delay(1);

SetHalfBridge(HalfBridgeId::U, HalfBridgeState::LOW_SIDE);
SetHalfBridge(HalfBridgeId::V, HalfBridgeState::DISABLED);
SetHalfBridge(HalfBridgeId::W, HalfBridgeState::HIGH_SIDE);
delay(1);

/*

  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  delay(5000);
  

  for(uint8_t hall_state_index = 0; hall_state_index < 6; hall_state_index++)
  {
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
      SetTrapizoidal(hall_states[hall_state_index][0], hall_states[hall_state_index][1], hall_states[hall_state_index][2], true, true);
      delay(1);
  }
  */
}
