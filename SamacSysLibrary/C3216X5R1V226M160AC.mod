PCBNEW-LibModule-V1  2021-10-05 16:43:34
# encoding utf-8
Units mm
$INDEX
C3216_Commercial
$EndINDEX
$MODULE C3216_Commercial
Po 0 0 0 15 615c72a6 00000000 ~~
Li C3216_Commercial
Cd C3216_Commercial
Kw Capacitor
Sc 0
At SMD
AR 
Op 0 0 0
T0 0 0 1.27 1.27 0 0.254 N V 21 N "C**"
T1 0 0 1.27 1.27 0 0.254 N I 21 N "C3216_Commercial"
DS -1.6 -0.8 1.6 -0.8 0.2 24
DS 1.6 -0.8 1.6 0.8 0.2 24
DS 1.6 0.8 -1.6 0.8 0.2 24
DS -1.6 0.8 -1.6 -0.8 0.2 24
DS -4.1 -2 3.2 -2 0.05 24
DS 3.2 -2 3.2 2 0.05 24
DS 3.2 2 -4.1 2 0.05 24
DS -4.1 2 -4.1 -2 0.05 24
DC -3 0 -3.05 0 0.254 21
$PAD
Po -1.65 0
Sh "1" R 1.1 1.35 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.65 0
Sh "2" R 1.1 1.35 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE C3216_Commercial
$EndLIBRARY
